#!/usr/bin/env fish
umask 0027
test "$container" != oci; and status --is-login; and status --is-interactive; and exec byobu-launcher
